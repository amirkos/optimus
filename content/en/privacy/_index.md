---
title: "Privacy"
headless: true
---


General conditions
General Conditions for Personal Data Protection


This privacy statement applies to the collection and processing of personal information, and the use of cookies on web sites.
The terms of protection of personal data refer to the personal information that is located and which is processed, collected and stored by the use of the website.
Please read these General Terms and Conditions for Personal Data Care. By submitting your personal information through the Website, you acknowledge that you have read, understood and agree to collect, process and use your personal information in accordance with these General Terms.

1) Personal information that is collected and use of personal information

Access to the website is free and no registration is required.

In order to maintain the functionality of the Website and to provide the services that users request, personal information of the User may be collected and used on the Website.

Personal information is considered to be all identifiable information and information required for communication (eg surname and first name, residence address, email address, contact numbers of telephone, mobile and fax machines, etc.). They will only be collected if they are voluntarily provided by the user through this website, e-mail or otherwise.

Your personal information is used to enable you to use activities through the Website, to contact you or to resolve any problems.

You may, at any time, have the right of access, the right to rectify, amend or delete the data provided, and have the right to waive the given permission to process your data and to request the termination of their further processing.


2) Use of Cookies

To make it easier for us to use our site, we use cookies that allow us to identify users. Cookies are small pieces of data that are temporarily stored on your hard drive. Cookies do not contain personally identifiable information, so your privacy is protected. Each time you access the Website, our system will search your cookie for previously recorded information.

Cookies can be classified as:
Session cookies - are stored on your computer and are automatically deleted when you close your web browser. They allow the website to receive temporary information, such as comments or the status of a shopping cart.
Persistent cookies - They remain in your web browser even after closing and usually have an expiration date. The website uses persistent cookies to facilitate access for registered users. They store information such as your username and password, which allows you to use the Remember Me functionality when logging in, so you do not have to enter your username and password every time you visit the website.
First-party cookie - These come from the website you visited and may be temporary or permanent. They allow websites to save information that is used when a user revisits the website.
Third-party cookies - There are several third-party services that store limited cookies (Facebook, Instagram, Google Analytics and AdWords) for the user.

What options are available to you?
In browser settings such as Internet Explorer, Safari, Firefox, or Chrome, you can specify which cookies you want to accept and which cookies to reject. The place where you can find the settings depends on the type of your browser. Use the "help" option in your browser to find the settings you need.
If you choose not to accept certain cookies, you may not be able to use certain features on our site.

3) Website security

All customer information is strictly kept and is only available to employees who need it. We will store your personal information securely and take all precautions to prevent the loss, misuse or alteration of your information. Our websites are maintained regularly and strive to protect against any unauthorized use. Although we use security measures to ensure that your personal information is fully protected, please be aware that there is always a certain risk and that unauthorized persons or applications may bypass security barriers and intercept any activity on our sites to damage you and us.

4) Terms and Conditions

We reserve the right to modify, extend or shorten this privacy policy from time to time. Changes to this privacy policy will always be made public on our website.

At each visit to our site, we believe that you fully agree to the terms set out here. If you do not agree to these terms, please stop using our website.