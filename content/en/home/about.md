---
title: "About"
image: "profile.jpg"
weight: 0
---

Optimus computer programming and consultancy businesses
was founded in 2019.

Although it is founded recently, owner, and associates have over 10 years of programming experience,
consulting, administration and other computer related activities.

Enhance your business and expand your target market through our:

- web application
- custom software solutions
- mobile applications.

Particular emphasis is placed on ease of use and the ability to use on different devices (cell phones, tablets, laptops, ..).
