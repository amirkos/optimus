---
title: "O nama"
image: "profile.jpg"
weight: 0

resources:
- src: 'resume/AK.pdf'
  name: Instruction Guide
  params:
    ref: '90564568'
---
Optimus obrt za računalno programiranje i savjetovanje u vezi s računalima 
osnovan je  2019 g.

Iako mlad obrt, vlasnik i suradnici imaju preko 10 godina iskustva u programiranju, 
savjetovanju, administriranju i ostalim djelatnostima vezanim uz računala.

Unaprijedite svoje poslovanje i proširite ciljano tržište putem naših:

- web aplikacija
- programskih rješenja po narudžbi
- mobilnih aplikacija. 

Poseban naglasak stavljamo na lakoću upotrebe i mogućnost korištenja na različitim uređajima (mobiteli, tableti, laptopi,..).



