---
title: "Konzultacije"
weight: 3
---

## Naša misija
U svakom slučaju želimo ponuditi najoptimalnije rješenje prema stvarnim potrebama klijenta. Čovjek i njegov posao su nam na prvom mjestu, a zadovoljstvo isporučenom uslugom mora biti obostrano.

## Naša Vizija
Pričati razumljivim jezikom o nerazumljivim stvarima. Nastojati približiti informacijsko društvo i njegove benefite običnom čovjeku i njegovom "običnom" businessu. 

Imamo ekipu stručnih suradnika i partnerskih tvrtki s kojima uspješno surađujemo niz godina i s kojima smo implementirali vrlo kompleksna IT rješenja. 

Optimus, obrt za računalno programiranje objedinio je sva znanja i isustva stečena kroz godine rada na izazovnim projektima i ista vam sada nudimo kako bi vi unaprijedili svoje poslovanje i olakšali si neke poslovne procese. 

Obratite nam se, sigurni smo da imamo rješenje za vas