---
title: "Razvoj software-a"
weight: 2

---
Treba Vam specifično programsko rješenje? 

Nudimo Vam usluge projektiranja i razvoja softvera za bilo koji segment Vašeg poslovanja.

Full stack development

- JEE (Java enterprise edition) 
- Spring
- Hibernate
- Javascript, React, Angular
- HTML, CSS, SCSS 
- Cloud tehnologije (AWS,Docker)

Obratite nam se s povjerenjem, voljeli bih smo Vas prvo saslušati.

Potom Vaše ideje, implementiramo uz naše konzultacije i stručnost a sve u svrhu postizanja

maksimalnog unaprijeđenja vašeg poslovanja.

