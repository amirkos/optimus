---
title: "Web-Dizajn"
weight: 1
---

Atraktivan web dizajn prvi je korak prema približavanju ciljanoj skupini klijenata. 
Istaknite se u odnosu na konkurenciju dizajnom koji prati svjetske trendove!

Tehnologije koje koristimo:

- HTML 5
- CSS
- SCSS
- Javascript
- CMS (Hugo)
