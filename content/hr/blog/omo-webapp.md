---
title: "Web aplikacija za osobe s poteškoćama u čitanju"
date: 2021-06-25T13:28:33+01:00
tags: ["javascript", "projects", "mongo-db","react"]
---

Izrada web aplikacije za Omolab-komunikacije d.o.o.


## Korištene tehnologije:
- React
- Node.js (express.js)
- Mongo-db 
- Tailwind.css 

# Demo beta verzije
- https://omolab-web-client.herokuapp.com/






