---
title: "Golang"
date: 2022-02-25T13:28:33+01:00
tags: ["golang", "bears", "tutorials"]
---


#### ... nice video about `goolang` 


{{< youtube rKnDgT73v8s >}}

#### .. `go` in 12 minutes :)

{{< youtube C8LgvuEBraI >}}

#### or 100 seconds

{{< youtube 446E-r0rXHI >}}


![I GOT ONLY ONE GEAR AND THAT'S GO!!](https://www.azquotes.com/picture-quotes/quote-i-have-one-speed-i-have-one-gear-go-charlie-sheen-26-90-93.jpg)



