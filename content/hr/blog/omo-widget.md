---
title: "Omo-widget"
date: 2021-05-25T13:28:33+01:00
tags: ["javascript", "projects"]
---

Izrada widgeta za transformaciju webova za usklađivanje s europskom direktivom o pristupačnosti.
Widget je instaliran na ovom web-u te ga se može vidjeti u gornjem desnom uglu ekrana.

Korištene tehnologije:
- vanilla javascript



