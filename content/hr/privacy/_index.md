---
title: "Privatnost"
headless: true
---


Opći uvjeti
Opći uvjeti zaštite osobnih podataka


Izjava o zaštiti privatnosti odnosi se na prikupljanje i obradu osobnih podataka te primjenu kolačića (cookies) na internetskim stranicama.
Uvjeti zaštite osobnih podataka odnose se na osobne podatke koji se nalaze i koji se korištenjem web stranice obrađuju, odnosno prikupljaju i pohranjuju.
Pažljivo pročitajte ove opće uvjete zaštite osobnih podataka. Davanjem vaših osobnih podataka putem web stranice, potvrđujete da ste pročitali, razumjeli te da pristajete na prikupljanje, obradu i korištenje vaših osobnih podataka u skladu sa ovim općim uvjetima. 

1) Osobni podatci koji se prikupljaju i uporaba osobnih podataka

Pristup web stranici  je slobodan i ne traži se registracija.

Kako bi se održala funkcionalnost internetskih stranica i omogućila usluge koje korisnici traže, na internetskoj stranici mogu se prikupljati i upotrebljavati osobni podatci korisnika. 

Osobnim podacima smatraju se svi podaci kojima se utvrđuje identitet te podaci potrebni za komunikaciju (npr. prezime i ime, adresa stanovanja, adresa e-pošte, kontakt brojevi telefona, mobilnih i faks uređaja i sl). Isti će biti prikupljani isključivo ako ih korisnik dobrovoljno dostavi putem ove web stranice, elektroničke pošte ili na bilo koji drugi način. 

Vaši osobni podatci se koriste kako bi vam omogućili korištenje aktivnosti putem Internet stranice, kako bi uspostavili kontakt s vama ili riješili neke probleme.

U svakom trenutku imate pravo uvida, pravo ispravka, dopune ili brisanja datih podataka te pravo odustati od date privole za obradu Vaših podataka i zatražiti prestanak njihove daljnje obrade. 


2) Uporaba kolačića (Cookies)

Kako bismo olakšali korištenje naše web stranice, koristimo "Cookies " koji omogućuju identifikaciju korisnika. Cookies su male cjeline podataka, koje se privremeno pohranjuju na tvrdom disku.  Kolačići ne sadrže osobne podatke, tako da je vaša privatnost zaštićena. Prilikom svakog sljedećeg pristupa na Internet stranicu naš sustav će pretraživati vaš kolačić tražeći prethodno zapisane podatke. 

Kolačići se mogu klasificirati kao:
Privremeni kolačići (Session cookies) - pohranjuju se na računalu , te se automatski brišu nakon zatvaranja web-preglednika. Omogućuju internetskoj stranici da dobije privremene podatke, poput komentara ili stanja košarice za kupnju.
Trajni kolačići (Persistent cookies) - Ostaju u vašem web pregledniku i nakon zatvaranja i obično imaju rok isteka. Web stranica koristi trajne kolačiće kako bi olakšali pristup registriranim korisnicima. Pohranjuju podatke kao što su korisničko ime i lozinka, što omogućuje da koristite funkcionalnost „zapamti me“ prilikom prijavljivanja, tako da ne morate upisivati korisničko ime i lozinku svaki puta kada posjećujete web stranicu.
Kolačić od prve strane - oni dolaze s web stranice koju ste posjetili, a mogu biti privremeni ili stalni. Omogućavaju web stranicama da spreme podatke koji se koriste kada korisnik ponovno posjeti web stranicu. 
Kolačići od treće strane - postoji nekoliko vanjskih servisa koji korisniku spremaju limitirane kolačiće (Facebook, Instagram, Google Analytics i AdWords).

Koje su Vam opcije dostupne?
U postavkama pretraživača kao što su Internet Explorer, Safari, Firefox ili Chrome možete odrediti koje kolačiće želite prihvatiti a koje ćete odbiti. Mjesto na kojem možete pronaći postavke ovisi o vrsti Vašeg pretraživača. Putem opcije „pomoć“ u Vašem pretraživaču pronađite postavke koje trebate.
Ukoliko odaberete opciju da ne želite prihvatiti određene kolačiće, možda nećete moći koristiti određene funkcije na našoj Internet stranici.

3) Sigurnost web stranica

Svi se podaci o korisnicima strogo čuvaju i dostupni su samo djelatnicima kojima su ti podaci nužni za obavljanje posla. Vaše osobne podatke pohraniti ćemo na siguran način te ćemo poduzeti sve mjere opreza kako spriječili gubitak, zlouporaba ili promjena podataka. Naše internet stranice održavaju se redovito i nastoje se zaštititi od svakog neovlaštenog korištenja. Iako koristimo sigurnosne mjere kako bi vaši osobni podatci bili u potpunosti zaštićeni, imajte na umu da uvijek postoji određeni rizik te da neovlaštene osobe ili aplikacije mogu zaobići sigurnosne zapreke te presresti svaku aktivnost na našim internet stranicama kako bi oštetili i vas i nas.

4) Uvjeti i izmjene

Zadržavamo pravo da s vremena na vrijeme modificiramo, proširujemo ili skraćujemo ovaj dokument o zaštiti privatnosti. Promjene u ovom dokumentu o zaštiti privatnosti uvijek će biti javno objavljene na našim internet stranicama.

Pri svakom posjetu našoj stranici smatramo da se u potpunosti slažete s ovdje navedenim uvjetima. Ukoliko se ne slažete s navedenim uvjetima molimo vas da prestanete koristiti našu web stranicu.