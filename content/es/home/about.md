---
title: "Sobre"
image: "profile.jpg"
weight: 0
---

Optimus Craft para programación informática y consultoría informática
fue fundado en 2019.

Aunque un joven artesano, propietario y asociado tiene más de 10 años de experiencia en programación,
consultoría, administración y otras actividades relacionadas con la informática.

Mejore su negocio y expanda su mercado objetivo a través de:

- aplicación web
- soluciones de software a medida
- aplicaciones móviles.

Se hace especial hincapié en la facilidad de uso y la capacidad de usar en diferentes dispositivos (teléfonos celulares, tabletas, computadoras portátiles, ..).
