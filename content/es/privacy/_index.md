---
title: "Privacidad"
headless: true
---


Condiciones generales
Condiciones generales para la protección de datos personales


Esta declaración de privacidad se aplica a la recopilación y el procesamiento de información personal, y al uso de cookies en sitios web.
Los términos de protección de datos personales se refieren a la información personal que se encuentra y que se procesa, recopila y almacena mediante el uso del sitio web.
Lea estos Términos y condiciones generales para el cuidado de datos personales. Al enviar su información personal a través del sitio web, usted reconoce que ha leído, entendido y acepta recopilar, procesar y usar su información personal de acuerdo con estos Términos Generales.

1) Información personal que se recopila y uso de información personal

El acceso al sitio web es gratuito y no es necesario registrarse.

Para mantener la funcionalidad del sitio web y proporcionar los servicios que solicitan los usuarios, la información personal del usuario se puede recopilar y utilizar en el sitio web.

Se considera que la información personal es toda la información identificable y la información requerida para la comunicación (por ejemplo, apellido y nombre, dirección de residencia, dirección de correo electrónico, números de contacto de teléfonos, máquinas móviles y de fax, etc.). Solo se recopilarán si el usuario los proporciona voluntariamente a través de este sitio web, correo electrónico u otro medio.

Su información personal se utiliza para permitirle usar actividades a través del sitio web, contactarlo o resolver cualquier problema.

Puede, en cualquier momento, tener el derecho de acceso, el derecho de rectificar, modificar o eliminar los datos proporcionados, y el derecho de renunciar al permiso otorgado para procesar sus datos y solicitar la terminación de su procesamiento posterior.


2) Uso de cookies

Para facilitarnos el uso de nuestro sitio, utilizamos cookies que nos permiten identificar a los usuarios. Las cookies son pequeñas piezas de datos que se almacenan temporalmente en su disco duro. Las cookies no contienen información de identificación personal, por lo que su privacidad está protegida. Cada vez que accede al sitio web, nuestro sistema buscará en su cookie información previamente registrada.

Las cookies se pueden clasificar como:
Cookies de sesión: se almacenan en su computadora y se eliminan automáticamente cuando cierra su navegador web. Permiten que el sitio web reciba información temporal, como comentarios o el estado de un carrito de compras.
Cookies persistentes: permanecen en su navegador web incluso después del cierre y generalmente tienen una fecha de vencimiento. El sitio web utiliza cookies persistentes para facilitar el acceso de los usuarios registrados. Almacenan información como su nombre de usuario y contraseña, lo que le permite usar la funcionalidad Recordarme al iniciar sesión, por lo que no tiene que ingresar su nombre de usuario y contraseña cada vez que visita el sitio web.
Cookies de origen: provienen del sitio web que visitó y pueden ser temporales o permanentes. Permiten a los sitios web guardar información que se utiliza cuando un usuario vuelve a visitar el sitio web.
Cookies de terceros: existen varios servicios de terceros que almacenan cookies limitadas (Facebook, Instagram, Google Analytics y AdWords) para el usuario.

¿Qué opciones están disponibles para usted?
En la configuración del navegador, como Internet Explorer, Safari, Firefox o Chrome, puede especificar qué cookies desea aceptar y qué cookies rechazar. El lugar donde puede encontrar la configuración depende del tipo de su navegador. Use la opción "ayuda" en su navegador para encontrar la configuración que necesita.
Si elige no aceptar ciertas cookies, es posible que no pueda usar ciertas funciones en nuestro sitio.

3) seguridad del sitio web

Toda la información del cliente se mantiene estrictamente y solo está disponible para los empleados que la necesitan. Almacenaremos su información personal de forma segura y tomaremos todas las precauciones para evitar la pérdida, mal uso o alteración de su información. Nuestros sitios web se mantienen regularmente y se esfuerzan por proteger contra cualquier uso no autorizado. Aunque utilizamos medidas de seguridad para garantizar que su información personal esté completamente protegida, tenga en cuenta que siempre existe un cierto riesgo y que personas o aplicaciones no autorizadas pueden pasar por alto las barreras de seguridad e interceptar cualquier actividad en nuestros sitios para dañarlo a usted y a nosotros.

4) Términos y condiciones

Nos reservamos el derecho de modificar, ampliar o acortar esta política de privacidad de vez en cuando. Los cambios a esta política de privacidad siempre se harán públicos en nuestro sitio web.

En cada visita a nuestro sitio, creemos que está totalmente de acuerdo con los términos establecidos aquí. Si no está de acuerdo con estos términos, deje de usar nuestro sitio web.