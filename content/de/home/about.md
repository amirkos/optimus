---
title: "Über mich"
image: "profile.jpg"
weight: 0
---

Optimus Craft für Computerprogrammierung und Computerberatung
wurde 2019 gegründet.

Obwohl ein junger Handwerker, Eigentümer und Mitarbeiter über 10 Jahre Programmiererfahrung verfügt,
Beratung, Verwaltung und andere mit dem Computer zusammenhängende Tätigkeiten.

Verbessern Sie Ihr Geschäft und erweitern Sie Ihren Zielmarkt durch:

- Webanwendung
- Kundenspezifische Softwarelösungen
- mobile Anwendungen.

Besonderes Augenmerk wird auf die Benutzerfreundlichkeit und die Fähigkeit zur Verwendung auf verschiedenen Geräten (Handys, Tablets, Laptops, ..) gelegt.

