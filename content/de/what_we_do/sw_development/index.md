---
title: "Software-Entwicklung"
weight: 2
resources:
    - src: plant.jpg
      params:
          weight: -100
---

Benötigen Sie eine spezielle Softwarelösung?

Wir bieten Software-Design- und -Entwicklungsservices für jedes Segment Ihres Unternehmens.

Full-Stack-Entwicklung

- JEE (Java Enterprise Edition)
- Spring 
- Hibernate
- Javascript, Reagieren, Angular
- HTML, CSS, SCSS
- Cloud-Technologien (AWS, Docker)

Kontaktieren Sie uns mit Vertrauen, wir würden uns freuen, zuerst von Ihnen zu hören.

Ihre Ideen setzen wir dann mit Beratung, Leidenschaft und Know-how zum Zweck der Erreichung um

Maximierung Ihres Geschäfts.
