---
title: "Beratung"
weight: 3
---

## Unsere Mission
In jedem Fall möchten wir die bestmögliche Lösung für die tatsächlichen Bedürfnisse des Kunden anbieten. Der Mensch und sein Geschäft stehen an erster Stelle, und die Zufriedenheit mit der erbrachten Leistung muss auf Gegenseitigkeit beruhen.

## Unsere Vision
Sprechen Sie in verständlicher Sprache über unverständliche Dinge. Versuchen Sie, die Informationsgesellschaft und ihre Vorteile dem normalen Menschen und seinem "normalen" Geschäft näher zu bringen.

Wir haben ein Team von professionellen Mitarbeitern und Partnerunternehmen, mit denen wir seit vielen Jahren erfolgreich zusammenarbeiten und mit denen wir sehr komplexe IT-Lösungen implementiert haben.

Optimus, ein Unternehmen für Computerprogrammierung, hat alle Kenntnisse und Erfahrungen aus jahrelanger Arbeit an herausfordernden Projekten zusammengetragen. Wir bieten diese nun an, um Sie bei der Verbesserung Ihres Geschäfts und der Vereinfachung einiger Geschäftsprozesse zu unterstützen.

Kontaktieren Sie uns, wir haben bestimmt die Lösung für Sie