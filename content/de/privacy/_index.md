---
title: "Privatleben"
headless: true
---

Allgemeine bedingungen
Allgemeine Bedingungen für den Schutz personenbezogener Daten


Diese Datenschutzerklärung gilt für die Erhebung und Verarbeitung personenbezogener Daten sowie für die Verwendung von Cookies auf Websites.
Die Bestimmungen zum Schutz personenbezogener Daten beziehen sich auf die personenbezogenen Daten, die durch die Nutzung der Website erfasst, verarbeitet und gespeichert werden.
Bitte lesen Sie diese Allgemeinen Geschäftsbedingungen für die Pflege personenbezogener Daten. Durch die Übermittlung Ihrer persönlichen Daten über die Website bestätigen Sie, dass Sie Ihre persönlichen Daten gemäß diesen Allgemeinen Geschäftsbedingungen gelesen und verstanden haben und damit einverstanden sind, sie zu sammeln, zu verarbeiten und zu verwenden.

1) Personenbezogene Daten, die gesammelt werden, und Verwendung personenbezogener Daten

Der Zugang zur Website ist kostenlos und es ist keine Registrierung erforderlich.

Zur Aufrechterhaltung der Funktionalität der Website und zur Bereitstellung der von den Benutzern angeforderten Dienste können personenbezogene Daten des Benutzers gesammelt und auf der Website verwendet werden.

Als personenbezogene Daten gelten alle identifizierbaren Informationen und Informationen, die für die Kommunikation erforderlich sind (z. B. Vor- und Nachname, Wohnadresse, E-Mail-Adresse, Telefonnummern von Telefon-, Mobil- und Faxgeräten usw.). Sie werden nur gesammelt, wenn sie vom Benutzer freiwillig über diese Website, E-Mail oder auf andere Weise bereitgestellt werden.

Ihre persönlichen Daten werden verwendet, um Ihnen die Nutzung von Aktivitäten über die Website zu ermöglichen, Sie zu kontaktieren oder Probleme zu lösen.

Sie haben jederzeit das Recht auf Auskunft, Berichtigung, Änderung oder Löschung der bereitgestellten Daten sowie das Recht, auf die erteilte Erlaubnis zur Verarbeitung Ihrer Daten zu verzichten und die Einstellung ihrer weiteren Verarbeitung zu verlangen.


2) Verwendung von Cookies

Um die Nutzung unserer Website zu vereinfachen, verwenden wir Cookies, mit denen wir Benutzer identifizieren können. Cookies sind kleine Daten, die vorübergehend auf Ihrer Festplatte gespeichert werden. Cookies enthalten keine personenbezogenen Daten, sodass Ihre Privatsphäre geschützt ist. Bei jedem Zugriff auf die Website durchsucht unser System Ihren Cookie nach zuvor aufgezeichneten Informationen.

Cookies können klassifiziert werden als:
Sitzungscookies - werden auf Ihrem Computer gespeichert und automatisch gelöscht, wenn Sie Ihren Webbrowser schließen. Sie ermöglichen es der Website, temporäre Informationen wie Kommentare oder den Status eines Einkaufswagens zu erhalten.
Permanente Cookies - Sie verbleiben auch nach dem Schließen in Ihrem Webbrowser und haben normalerweise ein Ablaufdatum. Die Website verwendet dauerhafte Cookies, um den registrierten Benutzern den Zugang zu erleichtern. Sie speichern Informationen wie Ihren Benutzernamen und Ihr Kennwort, mit denen Sie die Funktion "Angemeldet bleiben" beim Anmelden verwenden können, sodass Sie Ihren Benutzernamen und Ihr Kennwort nicht jedes Mal eingeben müssen, wenn Sie die Website besuchen.
Erstanbieter-Cookie - Diese Cookies stammen von der von Ihnen besuchten Website und können vorübergehend oder dauerhaft sein. Sie ermöglichen es Websites, Informationen zu speichern, die verwendet werden, wenn ein Benutzer die Website erneut besucht.
Cookies von Drittanbietern - Es gibt verschiedene Drittanbieterdienste, die eingeschränkte Cookies (Facebook, Instagram, Google Analytics und AdWords) für den Benutzer speichern.

Welche Möglichkeiten stehen Ihnen zur Verfügung?
In Browsereinstellungen wie Internet Explorer, Safari, Firefox oder Chrome können Sie angeben, welche Cookies Sie akzeptieren und welche Cookies abgelehnt werden sollen. Die Stelle, an der Sie die Einstellungen finden, hängt vom Typ Ihres Browsers ab. Verwenden Sie die Option "Hilfe" in Ihrem Browser, um die gewünschten Einstellungen zu finden.
Wenn Sie bestimmte Cookies nicht akzeptieren, können Sie möglicherweise bestimmte Funktionen auf unserer Website nicht nutzen.

3) Website-Sicherheit

Alle Kundeninformationen werden streng aufbewahrt und stehen nur Mitarbeitern zur Verfügung, die sie benötigen. Wir werden Ihre persönlichen Daten sicher speichern und alle Vorkehrungen treffen, um den Verlust, den Missbrauch oder die Änderung Ihrer Daten zu verhindern. Unsere Websites werden regelmäßig gewartet und sind bestrebt, Sie vor unbefugter Nutzung zu schützen. Obwohl wir Sicherheitsmaßnahmen ergreifen, um sicherzustellen, dass Ihre persönlichen Daten vollständig geschützt sind, müssen Sie sich darüber im Klaren sein, dass immer ein bestimmtes Risiko besteht und dass nicht autorisierte Personen oder Anwendungen Sicherheitsbarrieren umgehen und jegliche Aktivitäten auf unseren Websites abfangen können, um Sie und uns zu beschädigen.

4) Allgemeine Geschäftsbedingungen

Wir behalten uns das Recht vor, diese Datenschutzerklärung von Zeit zu Zeit zu ändern, zu erweitern oder zu verkürzen. Änderungen dieser Datenschutzerklärung werden immer auf unserer Website veröffentlicht.

Wir sind der Ansicht, dass Sie bei jedem Besuch unserer Website den hier aufgeführten Bedingungen uneingeschränkt zustimmen. Wenn Sie diesen Bedingungen nicht zustimmen, beenden Sie bitte die Nutzung unserer Website.